package com.jiangge.utils;

import java.util.Arrays;
import java.util.List;

/**
 * 字符串处理工具类
 *
 * @author jiang.li
 * @date 2013-12-18 11:22
 * @modify qingzhao guo
 * @date 2014-11-22
 */
public class StringUtils {

    /**
     * 检查字符串是否为空
     *
     * @param str 字符串
     * @return
     */
    public static boolean isEmpty(String str) {
        if (str == null) {
            return true;
        } else if (str.length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 检查字符串是否为空
     *
     * @param str 字符串
     * @return
     */
    public static boolean isNotEmpty(String str) {
        if (str == null) {
            return false;
        } else if (str.length() == 0) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * 把字符串按分隔符转换为数组
     *
     * @param str  字符串
     * @param expr 分隔符
     * @return
     */
    public static String[] stringToArray(String str, String expr) {
        return str.split(expr);
    }

    /**
     * 将数组按照给定的分隔转化成字符串
     *
     * @param arr
     * @param expr
     * @return
     */
    public static String arrayToString(String[] arr, String expr) {
        String strInfo = "";
        if (arr != null && arr.length > 0) {
            StringBuffer sf = new StringBuffer();
            for (String str : arr) {
                sf.append(str);
                sf.append(expr);
            }
            strInfo = sf.substring(0, sf.length() - 1);
        }
        return strInfo;
    }


    /**
     * 将集合按照给定的分隔转化成字符串
     *
     * @param arr
     * @param expr
     * @return
     */
    public static String listToString(List<String> list, String expr) {
        String strInfo = "";
        if (list != null && list.size() > 0) {
            StringBuffer sf = new StringBuffer();
            for (String str : list) {
                sf.append(str);
                sf.append(expr);
            }
            strInfo = sf.substring(0, sf.length() - 1);
        }
        return strInfo;
    }

    /**
     * 将集合转换为String数组
     *
     * @param list 待转换的列表
     * @return String类型的数组
     * @author qingzhao guo
     */
    public static String[] listToStringArray(List<String> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        return list.toArray(new String[list.size()]);
    }

    /**
     * 将String数组转换为list集合
     *
     * @param strArr 待转换的String数组
     * @return List集合
     * @author qingzhao guo
     */
    public static List<String> stringArrayToList(String[] strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        return Arrays.asList(strArr);
    }

    /**
     * 所提供的字符串是否符合正则表达式
     *
     * @param str   待判断的字符串
     * @param regex 正则表达式
     * @return true：字符串与正则表达式匹配；false：字符串与正则表达式不匹配
     */
    public static boolean isMatchRegex(String str, String regex) {
        if (null == str || str.length() == 0) {
            return false;
        }
        if (null == regex || regex.length() == 0) {
            return false;
        }
        return str.matches(regex);
    }
}

